import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { CommonModule } from "@angular/common";
import { DestinoViaje } from '../models/destino-viaje.models';


@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  
})



export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje ;
  @HostBinding('attr.class') cssClass = 'col-md-4';

  constructor() {
    this.destino=new DestinoViaje('','');
   }
   

  ngOnInit(): void {
  }

}
